#!/bin/sh

echo "finding wallpaper"
wall=$(find $HOME/pix/wal -type f | shuf -n 1)

echo "setting it as wallpaer"
xwallpaper --zoom $wall

echo "makeing color scheme"
wal -i $wall >/dev/null

echo "remaking dwm"
(cd $HOME/.local/src/dwm && sudo make clean install >/dev/null)
echo "remaking dmenu"
(cd $HOME/.local/src/dmenu && sudo make clean install >/dev/null)

echo "restart dwm for changes to apply"
